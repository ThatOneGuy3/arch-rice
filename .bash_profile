#####################################
##          .Bash_Profile          ##
#####################################

[[ -f ~/.bashrc ]] && . ~/.bashrc

#       Does startx after you login in your tty, uncomment if you are NOT using a display manager
# [[ $(fgconsole 2>/dev/null == 1 ]] && exec startx -- vt1
