local wk = require("which-key")
local mappings = {
    q = {":q!<cr>", " Quit"},
    w = {":wq!<cr>", " Write And Quit"},
    x = {":bdelete<cr>", " Closes This Tab"},  
	f = {":Telescope find_files<cr>", " Telescope, Find A File"},
	r = {":Telescope live_grep<cr>", " Telescope, Live Grep"}
}
local opts = {prefix = '<leader>'}
wk.register(mappings, opts)
