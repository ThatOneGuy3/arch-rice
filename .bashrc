##############################
##       .BashRc File       ##
##############################

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

export HISTCONTROL=ignoreboth:erasedups

#   Exports

# Make neovim the default editor, change this to anything you want
export EDITOR='nvim'
export VISUAL='nvim'
export HISTCONTROL=ignoreboth:erasedups

#   The prompt

PS1='[\u@\h \W]\$ '

if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi

#   Ignore upper and lowercase when tab completion
bind "set completion-ignore-case on"

#   Aliases
alias v='nvim' # If you want editor to be neovim # Comment 1 out!
#alias v='vim' # If you want editor to be vim    # Comment 1 out!
# alias ls='ls -l'
alias ls='/home/ubuntu-rice/.cargo/bin/exa -a'
alias ll='ls -la'
alias h='cd ~/'
alias r='sudo cd /'

# Bash Prompt
# eval "$(starship init bash)"

#   Programs at startup

#pfetch
neofetch
#ufetch
#colorscripts random

